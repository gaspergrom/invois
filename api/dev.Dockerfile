ARG NODE_VERSION=16

FROM node:$NODE_VERSION-alpine AS dev
RUN apk add --no-cache git sed bash curl
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY .env .eslintrc.js nest-cli.json tsconfig.json tsconfig.build.json ./
COPY email ./email
COPY src ./src

EXPOSE 9090

ENV POSTGRES_DB=invois
ENV POSTGRES_USER=invois
ENV POSTGRES_PASSWORD=invois
ENV POSTGRES_PORT=5432
ENV POSTGRES_HOST=localhost
ENV JWT_SECRET=thisIsVeryAdvancedSecret

ENTRYPOINT ["npm", "run"]
CMD ["dev"]
