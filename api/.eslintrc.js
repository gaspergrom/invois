module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: false,
    },
    project: ['tsconfig.json'],
  },
  ignorePatterns: '**/*.factory.ts',
  extends: ['plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
  rules: {
    'prefer-template': 'warn',
    'no-console': 'error',
    'spaced-comment': 'warn',
    'no-magic-numbers': 'off',
    '@typescript-eslint/no-unused-vars': 'warn',
    '@typescript-eslint/no-extra-semi': 'off',
    '@typescript-eslint/explicit-function-return-type': 'warn',
    'import/prefer-default-export': 'off',
  },

};
