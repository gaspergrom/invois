import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectsService } from './projects.service';
import { ProjectsRepository } from './projects.repository';
import { ProjectsController } from './projects.controller';
import { Project } from './entities/project.entity';
import { repositoryProviderFactory } from '../shared/factories/repository-provider.factory';
import { Client } from '../clients/entities/client.entity';
import { ClientsRepository } from '../clients/clients.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Project])],
  providers: [
    ProjectsService,
    repositoryProviderFactory(Project, ProjectsRepository),
    repositoryProviderFactory(Client, ClientsRepository),
  ],
  controllers: [ProjectsController],
})
export class ProjectsModule {}
