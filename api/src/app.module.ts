import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './auth/strategies/jwt.strategy';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EmailService } from './shared/services/email.service';
import { GoogleStrategy } from './auth/strategies/google.strategy';
import { FacebookStrategy } from './auth/strategies/facebook.strategy';
import { LinkedinStrategy } from './auth/strategies/linkedin.strategy';
import { ClientsModule } from './clients/clients.module';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { User } from './auth/entities/user.entity';
import { Client } from './clients/entities/client.entity';
import { Project } from './projects/entities/project.entity';
import { ProjectsModule } from './projects/projects.module';
import { Transaction } from './transactions/entities/transaction.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const options: TypeOrmModuleOptions = {
          type: 'postgres',
          database: config.get<string>('POSTGRES_DB'),
          host: config.get<string>('POSTGRES_HOST'),
          port: +config.get<string>('POSTGRES_PORT'),
          username: config.get<string>('POSTGRES_USER'),
          password: config.get<string>('POSTGRES_PASSWORD'),
          synchronize: true,
          logging: false,
          migrations: ['src/migrations/**/*.ts'],
          entities: [User, Client, Project, Transaction],
        };
        return options;
      },
    }),
    PassportModule,
    AuthModule,
    ClientsModule,
    ProjectsModule,
  ],
  providers: [JwtStrategy, GoogleStrategy, FacebookStrategy, LinkedinStrategy, EmailService, ConfigService],
})
export class AppModule {}
