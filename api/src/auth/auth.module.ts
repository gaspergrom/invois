import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { AuthService } from './auth.service';
import { AuthRepository } from './auth.repository';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { EmailService } from '../shared/services/email.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { repositoryProviderFactory } from '../shared/factories/repository-provider.factory';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        secret: config.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: '24h' },
      }),
    }),
  ],
  providers: [AuthService, repositoryProviderFactory(User, AuthRepository), EmailService, ConfigService],
  controllers: [AuthController],
})
export class AuthModule {}
