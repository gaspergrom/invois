import { applyDecorators, createParamDecorator, ExecutionContext, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

interface AuthSettings {
  optional?: boolean;
}

class OptionalJwtAuthGuard extends AuthGuard('jwt') {
  handleRequest<UserDto>(_1: void, user: UserDto): UserDto {
    return user || undefined;
  }
}

export const AuthUser = createParamDecorator((data: never, context: ExecutionContext) => {
  const request = context.switchToHttp().getRequest();
  return request.user;
});

export const Auth = (settings: AuthSettings = {}): ((object, ClassDecorator, TypedPropertyDescriptor) => void) =>
  applyDecorators(
    UseGuards(settings.optional ? OptionalJwtAuthGuard : AuthGuard('jwt')),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized' }),
  );
