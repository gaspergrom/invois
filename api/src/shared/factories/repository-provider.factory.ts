import { Provider } from '@nestjs/common';
import { EntityManager, Repository } from 'typeorm';

export const repositoryProviderFactory = <T>(
  entity: new () => T,
  repository: new (...args: any[]) => Repository<T>,
): Provider<any> => ({
  provide: repository,
  inject: [EntityManager],
  useFactory: (entityManager: EntityManager) => new repository(entity, entityManager),
});
