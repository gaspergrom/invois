import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Client } from './entities/client.entity';
import { ClientsService } from './clients.service';
import { ClientsRepository } from './clients.repository';
import { ClientsController } from './clients.controller';
import { repositoryProviderFactory } from '../shared/factories/repository-provider.factory';

@Module({
  imports: [TypeOrmModule.forFeature([Client])],
  providers: [ClientsService, repositoryProviderFactory(Client, ClientsRepository)],
  controllers: [ClientsController],
})
export class ClientsModule {}
