ARG NODE_VERSION=16

#############################################
# DEPENDENCIES
#############################################
FROM --platform=$BUILDPLATFORM node:$NODE_VERSION-alpine AS base
ARG TARGETPLATFORM
RUN --mount=type=cache,id=var-cache-apk-$TARGETPLATFORM,target=/var/cache/apk,sharing=locked \
    --mount=type=cache,id=etc-apk-cache-$TARGETPLATFORM,target=/etc/apk/cache,sharing=locked \
    true \
    && apk add git sed \
    && mkdir -p /usr/src/app \
    && chown node:node /usr/src/app

RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    chown -R node:node "/home/node/.npm"

USER node
WORKDIR /usr/src/app
COPY --chown=node:node package*.json /usr/src/app/

USER root
RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    chown -R node:node "/home/node/.npm"
USER node

ENV NODE_OPTIONS="--enable-source-maps --max-old-space-size=8192 --no-warnings"
ENV CYPRESS_CACHE_FOLDER=/usr/src/app/.cache
RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    npm ci --production --prefer-offline --no-audit --loglevel notice --no-optional

#############################################
# BUILDER
#############################################
FROM base AS build

USER root
RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    env NODE_ENV=development npm install --prefer-offline --no-audit --loglevel notice
USER node

ENV NODE_OPTIONS="--enable-source-maps --max-old-space-size=8192 --no-warnings"
ENV CYPRESS_CACHE_FOLDER=/usr/src/app/.cache
COPY --chown=node:node cypress* /usr/src/app/cypress
RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    env NODE_ENV=development npm install --prefer-offline --no-audit --loglevel notice

COPY --chown=node:node .babelrc .eslintignore .eslintrc.js nuxt.config.js tailwind.config.js tsconfig.json plugin-types.d.ts /usr/src/app/
COPY --chown=node:node _eslint* /usr/src/app/_eslint
COPY --chown=node:node assets* /usr/src/app/assets
COPY --chown=node:node components* /usr/src/app/components
COPY --chown=node:node layouts* /usr/src/app/layouts
COPY --chown=node:node middleware* /usr/src/app/middleware
COPY --chown=node:node pages* /usr/src/app/pages
COPY --chown=node:node plugins* /usr/src/app/plugins
COPY --chown=node:node static* /usr/src/app/static
COPY --chown=node:node store* /usr/src/app/store
COPY --chown=node:node test* /usr/src/app/test

RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    npm run lint -- --quiet
RUN --mount=type=cache,id=npm,target=/home/node/.npm/,rw,uid=1000,gid=1000 \
    npm run build

##############################################
# RELEASE
##############################################
# TODO: setup docker for release

