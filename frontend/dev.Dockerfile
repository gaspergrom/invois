ARG NODE_VERSION=16

FROM node:$NODE_VERSION-alpine AS dev
RUN apk add --no-cache git sed bash curl

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install

COPY .env .babelrc .eslintignore .eslintrc.js nuxt.config.js tailwind.config.js tsconfig.json plugin-types.d.ts ./
COPY assets ./assets
COPY components ./components
COPY layouts ./layouts
COPY middleware ./middleware
COPY pages ./pages
COPY plugins ./plugins
COPY static ./static
COPY store ./store

EXPOSE 9001

ENV API_URL=/api/
ENV API_TARGET=http://api:9090
ENV HOST=0.0.0.0

ENTRYPOINT ["npm", "run"]
CMD ["dev"]
